
exports.septets_to_octets = (buf) ->
    ###
    Takes a Buffer containing data that was encoded in GSM septets; outputs
    a new Buffer containing the data expanded in ordinary octets.
    ###
    shift = (i) -> i%7
    lsb_to_take = (i) -> 7 - (shift i)
    residual_length = (i) -> 1 + (shift i)

    output = new Buffer(buf.length*8/7)
    output.length = 0
    residual = 0

    outputPokeAndInc = (data) -> output[output.length++] = data

    for i in [0...buf.length]
        c = ((buf[i] & ((1<<lsb_to_take i)-1)) << shift i)
        outputPokeAndInc c|residual
        residual = buf[i] >> (lsb_to_take i) 
        if (shift i) == 6
            outputPokeAndInc residual
            residual = 0
    return output

