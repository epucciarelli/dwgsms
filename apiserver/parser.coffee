require './bufferaddons'

bp = require '../binaryparser/'

util = require('util')

parserFactory = (init) ->
    init ?= {}
    pa = new bp.BinaryParser
    pa.setByteOrder('be')
    pa[k] = init[k] for k of init
    return pa

binaryContentToString = (text) ->
    if @encoding > 1
        throw new Error 'Invalid encoding #{@encoding}' 
    if @encoding is 1 # ascii
        text.swap16()
        return text.toString 'ucs2'
    return text.toString 'ascii'


exports.newParser = (handler) ->
    p = parserFactory {handler: handler}
    dwgErrors = [
        'succeed',
        'fail',
        'timeout',
        'bad request',
        'port unavailable',
        'partial succeed'
    ]

    dwgStatus = [
        'OK',
        'SIM missing',
        'Unregistered',
        'Unavailable'
    ]

    p.parsedMessages = 0

    p.setFormat {
        __repeat: 'forever',
        bodyLength: bp.dword(),
        macAddress: bp.bits(6),
        unused: bp.bits(2),
        time: bp.dword(),
        serial: bp.dword(),
        mType: bp.ushort(),
        flag: bp.ushort(),
        body: bp.bits('bodyLength'),
        onFinished: ->
            p.parsedMessages += 1
            header = this
            switch @mType
                when 0
                    console.log 'Keepalive received'
                    console.log 'Weird; keepalive has a body of length #{@bodyLength}' \
                        if @bodyLength isnt 0
                    p.handler.handleKeepalive this
                when 2
                    (parserFactory {handler: p.handler})
                        .setFormat({
                            result: bp.char8( (res_byte) ->
                                @result_desc =  if res_byte < dwgErrors.length \
                                    then dwgErrors[res_byte] \
                                    else 'other error: #{res_byte}'
                            ),
                            onFinished: ->
                                p.handler.handleSMSReqResponse this, header
                        }).addBuffer @body
                when 3
                    results = []
                    (parserFactory {handler: p.handler})
                        .setFormat({
                            destinations: bp.char8(),
                            status_block: {
                                __repeat: 'destinations',
                                dest: bp.bits(24, (n) ->
                                    i=0
                                    # count digits
                                    i++ while(i<24 and n[i] isnt 0)
                                    @dest = n.toString 'ascii', 0, i
                                ),
                                port: bp.char8(),
                                result: bp.char8( (res_byte) ->
                                    if res_byte<dwgErrors.length
                                        @result_desc = dwgErrors[res_byte]
                                    else
                                        @result_desc = 'other error: #{res_byte}'
                                ),
                                slice_count: bp.char8(),
                                successful_slices: bp.char8( ->
                                    # last field
                                    results.push {}
                                    for k, v of this
                                        results[results.length-1][k] = v
                                ),
                            },
                            onFinished: ->
                                p.handler.handleSMSResponse results, header
                        })
                        .addBuffer @body
                when 5
                    (parserFactory {handler: p.handler})
                        .setFormat({
                            sender: bp.bits(24, (n) ->
                                console.log 'Sender buffer:', n
                                i=0
                                # count digits
                                i++ while i<24 and n[i] isnt 0
                                @sender = n.toString 'ascii', 0, i
                            ),
                            type: bp.char8(),
                            port: bp.char8(),
                            raw_timestamp: bp.bits(15, (ts) ->
                                ts = ts.toString 'ascii'
                                @timestamp = new Date((ts.slice 0, 4), 
                                    (parseInt ts.slice 4, 6) - 1,
                                    (ts.slice 6, 8),
                                    (ts.slice 8, 10),
                                    (ts.slice 10, 12),
                                    (ts.slice 12, 14))
                            ),
                            timezone: bp.char8(),
                            encoding: bp.char8(),
                            length: bp.ushort(),
                            content: bp.bits('length', (text) ->
                                @text = binaryContentToString text
                            ),
                            onFinished: ->
                                p.handler.handleSMS this, header
                        })
                        .addBuffer @body
                when 7
                    status = []
                    (parserFactory {handler: p.handler})
                        .setFormat({
                            ports: bp.char8(),
                            status_bytes: {
                                __repeat: 'ports',
                                status_byte: bp.char8((byte) ->
                                    status.push {byte: byte, text:
                                        if byte < 4 \
                                        then (dwgStatus[byte]) else 'Unknown status #{byte}'
                                    }
                                )
                            },
                            onFinished: ->
                                @status = status
                                p.handler.handleStatus this, header
                        })
                        .addBuffer @body
                when 10
                    (parserFactory {handler: p.handler})
                        .setFormat({
                            result: bp.char8( (byte) -> 
                                @resultDesc = if byte < dwgErrors.length \
                                    then dwgErrors[byte] else "Unknown USSD result #{byte}"
                            ),
                            onFinished: ->
                                console.log 'ussd result finished'
                                p.handler.handleUSSDRequestResponse this, header
                        })
                        .addBuffer @body
                when 11
                    (parserFactory {handler: p.handler})
                        .setFormat({
                            port: bp.char8(),
                            status: bp.char8( (byte) ->
                                console.log 'WATCH, we\'re receiving a USSD response'
                                statusDescriptions = {
                                    0: 'No further user action required',
                                    1: 'Further user action required',
                                    2: 'USSD terminated by network',
                                    4: 'Operation not supported'
                                }
                                @statusDesc = if byte in statusDescriptions \
                                    then statusDescriptions[byte] \
                                    else "Unknown USSD result #{byte}"
                            ),
                            contentLength: bp.ushort((c) ->
                                console.log 'contentLength (in fieri)', c
                            ),
                            encoding: bp.char8((c) ->
                                console.log 'encoding', c
                            ),
                            content: bp.bits( 'contentLength', (text) ->
                                console.log 'content length:', @contentLength
                                @text = binaryContentToString text
                                console.log 'done:', @text
                            ),
                            onFinished: ->
                                console.log 'ussd response finished'
                                p.handler.handleUSSDResponse this, header
                        })
                        .addBuffer @body
                else console.log('Invalid type %d', @mType)
    }
