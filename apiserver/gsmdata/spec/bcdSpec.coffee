bcd_to_b = require('../bcd').bcd_to_b

describe 'BCD to integer converter', ->
	code = (string) -> string.charCodeAt(0)
	it 'can convert an ASCII digit into an integer', ->
		(expect bcd_to_b code '0').toBe 0
		(expect bcd_to_b code '8').toBe 8
	it 'can convert an ASCII uppercase hex digit into an integer', ->
		(expect bcd_to_b code 'B').toBe 11
		(expect bcd_to_b code 'F').toBe 15
