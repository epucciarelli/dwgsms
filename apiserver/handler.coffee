msgHelpers = require('./msghelpers')
parser_module = require('./parser');
Buffer = require('buffer').Buffer
BinaryBuilder = require('../binaryparser/examples/binary.builder').BinaryBuilder

exports.GWHandler = ->
	@_USSDSessionActive = false
	@_send = (data) ->
		@_socket.write data, 'binary'

	@buildMessage = (type, body, serial) ->
		bb = new BinaryBuilder 1024
		bb.addDword body.length
		bb.appendBuffer @_myMac
		bb.addShort 0
		(bb.addDword new Date().getTime()/1000)
		bb.addDword @_lastSerial
		bb.addShort type
		bb.addShort 0
		bb.appendBuffer body
		return bb.getBuffer()

	@receiveData = (data) =>
		(parser_module.newParser this).addBuffer data

	@init = (socket) ->
		@_socket = socket
		@handler = handler = this
		@_status = {}
		setTimeout(->
			setInterval(->
				handler._sendKeepalive()
			, 15000)
		, 4000)

	@_lastSerial = 0;
	@_myMac= new Buffer('\x00\x16\x3e\x1f\x67\x3a', 'binary')
	@_sentSMSRequests = {}
	@_sentUSSDRequests = {}

	@_sendKeepalive = ->
		@_send @buildMessage 0, new Buffer 0

	@handleKeepalive = ->
		console.log 'Our keepalive came back'

	@sendSMSRequest = (dest, content, callback) ->
		if dest.length > 100
			throw new Error 'Too many destination numbers'
		if content.length > 670
			throw new Error 'Content too long'
		bb = new BinaryBuilder 1024
		bb.addChar 255 # any port
		bb.addChar 0   # 0 = ASCII, 1= UNICODE
		bb.addChar 0   # 0 = SMS
		bb.addChar dest.length
		for d in dest
			bb.addString d
			bb.addChar 0 for i in [d.length..24]
		bb.addShort content.length
		bb.addString content
		body = bb.getBuffer()
		msg = @buildMessage 1, body
		@_sentSMSRequests[msgHelpers.bufToAsciiHex \
			msgHelpers.buildIDFromMsgBuffer msg ] = callback
		@_send msg

	@sendUSSDRequest = (content, callback, newSession) ->
		USSD_REQUEST_TYPE = 9
		port = 0
		console.log 'Callback is', callback
		fireRequest = =>
			bb = new BinaryBuilder 1024
			bb.addChar port
			bb.addChar 1 # Send
			bb.addShort(content.length)
			bb.addString(content)
			msg = @buildMessage USSD_REQUEST_TYPE, bb.getBuffer()
			@_send msg
			id = msgHelpers.buildIDFromMsgBuffer msg
			id_text = msgHelpers.bufToAsciiHex id
			console.log 'fireRequest: id is', id_text
			@_sentUSSDRequests[id_text] = callback
		if @_USSDSessionActive and newSession
			bb = new BinaryBuilder 1024
			bb.addChar port
			bb.addChar 2 # End session
			bb.addShort 0
			msg = @buildMessage USSD_REQUEST_TYPE, bb.getBuffer()
			@_send msg
			@_sentUSSDRequests[msgHelpers.bufToAsciiHex \
				msgHelpers.buildIDFromMsgBuffer msg] = fireRequest
			# TODO: check that End session worked fine
		else
			fireRequest()

	@handleUSSDRequestResponse = (status, header) ->
		id_text = msgHelpers.bufToAsciiHex msgHelpers.buildIDFromHeader header
		console.log status
		if not id_text in @_sentUSSDRequests
			console.log 'Received response to a request we did not sent. Ignoring'
			return
		if status.result isnt 0
			@_sentUSSDRequests[id_text] false, status
			delete @_sentUSSDRequests[id_text]

	@handleUSSDResponse = (status, header) ->
		id_text = msgHelpers.bufToAsciiHex msgHelpers.buildIDFromHeader header
		console.log status
		if not (id_text in @_sentUSSDRequests)
			console.log 'Received USSD but no header matches; no callback!'
			return
		console.log 'This id_text:', id_text
		console.log 'Stored USSD requests:', @_sentUSSDRequests
		console.log 'Callback to be called is', @_sentUSSDRequests[id_text]
		@_sentUSSDRequests[id_text] true, status
		delete @_sentUSSDRequests[id_text]

	@handleStatus = (status, header) ->
		console.log 'Received status message'
		@_status[status.__index] = status

	@handleStatusFinished = (header) ->
		bb = new BinaryBuilder 1024
		bb.addChar 0 # succeed
		msg = msgHelpers.buildMessageWithID 8, bb.getBuffer(), \
			msgHelpers.buildIDFromHeader header
		@_send msg
		console.log('Status reply sent');

	@handleSMSReqResponse = (status, header) ->
		console.log 'handler: got status', status.result_desc
		@_sentSMSRequests[msgHelpers.bufToAsciiHex \
			msgHelpers.buildIDFromHeader header] status

	@handleSMSResponse = (status, header) ->
		id = msgHelpers.buildIDFromHeader header
		@_sentSMSRequests[msgHelpers.bufToAsciiHex id] status
		@_send msgHelpers.buildMessageWithID 4, (new Buffer 1), id

	@handleSMS = (sms, header) ->
		console.log 'Cool, we got a SMS!'
		console.log sms
		bb = new BinaryBuilder 1024
		bb.addChar 0
		@_send msgHelpers.buildMessageWithID 6, bb.getBuffer(), \
			msgHelpers.buildIDFromHeader header

	@getStatus = ->
		return @_status

	this
