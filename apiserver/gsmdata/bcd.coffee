
zeroCode = 48
nineCode=57
hexOffset = 55

exports.bcd_to_b = (c) ->
    ###
    Takes an ASCII pointcode representing a decimal or hexadecimal uppercase
    digit; outputs the represented numeric value.
    ###
    return c-zeroCode if (c<=nineCode)
    c-hexOffset

