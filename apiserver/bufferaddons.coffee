
Buffer = require('buffer').Buffer

Buffer.prototype.swap16 = () ->
	###
	Swaps even and odd bytes inside the Buffer.
	###
	throw new TypeError('Cannot swap if the buffer length is not even') \
		if this.length % 2 != 0
	swapWithNext = (i) ->
		t = this[i]
		this[i] = this[i+1]
		this[i+1] = t
	swapWithNext i for i in [0...b.length] by 2

Buffer.prototype.append = (inbuf) ->
	outbuf = new Buffer(this.length+inbuf.length)
	this.copy(outbuf)
	inbuf.copy(outbuf, this.length)
	return outbuf