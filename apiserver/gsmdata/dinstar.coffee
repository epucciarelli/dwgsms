bcd_to_b = require('./bcd').bcd_to_b

exports.dinstar_to_binary = (inputbuf) ->
    ###
    Takes a Buffer containing a binary string, as returned by a Dinstar
    gateway in lieu of alphanumeric sender data, and outputs a Buffer
    containg the binary data sent by the network.
    ###
    new Buffer(
        ( \
            (bcd_to_b inputbuf[i*2]) \
            | ((bcd_to_b inputbuf[i*2+1]) << 4) \
            for i in [0...inputbuf.length/2])
    )

