describe 'Dinstar SMS API message parser', ->
	parsermodule = require '../parser'
	newParser = parsermodule.newParser
	Buffer = (require 'buffer').Buffer
	require '../bufferaddons'
	# A message ID is composed of 16 bytes
	message_sample_id = new Buffer '\x2a\0\0\0\0\x2a\0\0\0\0\0\0\0\0\0\0', 'binary'
	(expect message_sample_id.length).toBe 16
	msg = (type, body) -> 
		return (require '../msghelpers').buildMessageWithID(type, (new Buffer(body, 'binary')), 
			message_sample_id)

	mockHandler = -> {
		genericCallback: (parserStatus, header) ->
			@parserStatus = parserStatus
			@header = header
	}

	handler = new mockHandler
	parser = newParser(handler)

	runCase = (functionName, msgtype, msgbody) ->
		delete handler[k] for k in handler if k isnt 'genericCallback'
		handler[functionName] = handler.genericCallback
		(spyOn handler, functionName).andCallThrough()
		parser.addBuffer(msg msgtype, msgbody)
		(expect handler[functionName]).toHaveBeenCalled()
		return handler.parserStatus

	it 'can parse a keepalive message', ->
		runCase 'handleKeepalive', 0, ''

	it 'can parse a success status message', ->
		s = runCase 'handleStatus', 7, '\x01\0'
		(expect s.ports).toBe s.status.length
		(expect s.ports).toBe 1
		(expect s.status[0].byte).toBe 0

	it 'can parse a status message for multiple ports', ->
		s = runCase 'handleStatus', 7, '\x04\0\0\x03\0'
		statuses = [0,0,3,0]
		(expect s.ports).toBe s.status.length
		(expect s.ports).toBe 4
		for port in [0..3]
			(expect s.status[port].byte).toBe statuses[port]

	it 'can parse a SMS received message', ->
		s = runCase 'handleSMS', 5, '123456123456\0\0\0\0\0\0\0\0\0\0\0\0' +\
			'\0\x0220120101123456 \0\0\0\x06abcdef'
		(expect s.sender).toBe '123456123456'
		(expect s.port).toBe 2
		(expect s.timestamp).toEqual new Date 2012, 0, 1, 12, 34, 56
		(expect s.text).toBe 'abcdef'

	describe 'can parse a Send SMS Request Response (right after the request)', ->
		runSmsReqRespCase = (body) -> runCase 'handleSMSReqResponse', 2, body
		it 'successful', ->
			s = runSmsReqRespCase '\0'
			(expect s.result).toBe 0
			(expect s.result_desc).toBeDefined
		it 'unsuccessful', ->
			s = runSmsReqRespCase '\x01'
			(expect s.result).toBe 1
			(expect s.result_desc).toBeDefined

	describe 'can parse a Send SMS Response (after sending)', ->
		runSmsRespCase = (body) ->
			runCase 'handleSMSResponse', 3, body
		it 'successful for a single destination', ->
			s = runSmsRespCase '\x01' + \
				'123456123456\0\0\0\0\0\0\0\0\0\0\0\0' + \
				'\x01\0\x01\x01'
			(expect s.length).toBe 1
			s = s[0]
			(expect s.port).toBe 1
			(expect s.result).toBe 0
			(expect s.slice_count).toBe 1
			(expect s.successful_slices).toBe 1
			(expect s.dest).toBe '123456123456'

		it 'multiple destinations', ->
			s = runSmsRespCase '\x02' + \
				'123456123456\0\0\0\0\0\0\0\0\0\0\0\0' + \
				'\x01\0\x01\x01' + \
				'123456123457\0\0\0\0\0\0\0\0\0\0\0\0' + \
				'\x01\0\x01\x01'
			(expect s.length).toBe 2
			for i in s
				(expect i.port).toBe 1
				(expect i.result).toBe 0
				(expect i.slice_count).toBe 1
				(expect i.successful_slices).toBe 1
			(expect s[0].dest).toBe '123456123456'
			(expect s[1].dest).toBe '123456123457'

	describe 'can parse messages delivered in non-standard chunks:', ->
		myMessage = null
		oldHandler = null

		it '[test must be set up now]', ->
			myMessage = msg 0, ''
			myMessage = myMessage.append msg 0, ''
			myMessage = myMessage.append msg 7, '\x04\0\0\0\0'
			oldHandler = parser.handler

		it 'can parse a chunk with two messages', ->
			parser.handler = createSpyObj 'handlerSpy', [
				'handleKeepalive', 'handleStatus']
			parser.addBuffer myMessage
			(expect parser.handler.handleKeepalive).toHaveBeenCalled()
			(expect parser.handler.handleStatus).toHaveBeenCalled()

		it 'can parse one or more messages delivered in multiple chunks', ->
			parser.handler = createSpyObj 'handlerSpy', [
				'handleKeepalive', 'handleStatus']
			parser.addBuffer myMessage.slice 0, 22
			(expect parser.handler.handleKeepalive).not.toHaveBeenCalled()
			# Keepalive has an empty body, so its length is 24
			parser.addBuffer myMessage.slice 22, 27
			(expect parser.handler.handleKeepalive).toHaveBeenCalled()
			(expect parser.handler.handleStatus).not.toHaveBeenCalled()
			parser.addBuffer myMessage.slice 27
			(expect parser.handler.handleStatus).toHaveBeenCalled()

		it '[test can be disposed of now]', ->
			parser.handler = oldHandler
			console.log 'parsed messages:', parser.parsedMessages

