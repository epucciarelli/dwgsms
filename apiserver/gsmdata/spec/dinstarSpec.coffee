dinstar = require('../dinstar')
septets = require('../septets')
vodafone_string = undefined

describe 'Dinstar buffer converter', ->
	it 'can convert a Dinstar ASCII data buffer into binary GSM data', ->
		vodafone_string = dinstar.dinstar_to_binary new Buffer '6D7393C6E7BBBC'
		(expect vodafone_string.toString 'binary').toBe '\xd6\x37\x39\x6c\x7e\xbb\xcb'

describe 'GSM septet converter', ->
	it 'can convert GSM septets into octets', ->
		(expect (septets.septets_to_octets vodafone_string).toString())
			.toBe 'Vodafone'
