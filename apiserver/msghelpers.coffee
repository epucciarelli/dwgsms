BinaryBuilder = require('../binaryparser/examples/binary.builder').BinaryBuilder

exports.buildMessageWithID = (type, body, id) ->
	bb = new BinaryBuilder 1024
	bb.addDword body.length
	bb.appendBuffer id
	bb.addShort type
	bb.addShort 0
	bb.appendBuffer body
	return bb.getBuffer()

exports.buildIDFromMsgBuffer = (buffer) ->
	buffer.slice 4, 20

exports.buildIDFromHeader = (header) ->
		id = new BinaryBuilder 1024
		id.appendBuffer header.macAddress
		id.appendBuffer header.unused
		id.addDword header.time
		id.addDword header.serial
		return id.getBuffer()

exports.bufToAsciiHex = (buffer) ->
	buf = new Array
	for i in [0...buffer.length]
		buf.push '0' if buffer[i] < 16
		buf.push buffer[i].toString 16
	return buf.join ''