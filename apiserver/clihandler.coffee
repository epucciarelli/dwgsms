
StringScanner = require('strscan').StringScanner

exports.CLIHandler = ->

	@init = (output, handler) ->
		@_output = output
		@_handler = handler

	@receiveLine = (line) ->
		line = line.toString().trim()
		scanner = new StringScanner line
		getWord = ->
			r = scanner.scan /\S+/
			scanner.scan /\s+/
			return r

		command = getWord()

		switch command
			when 'sms'
				dest = getWord()
				content = scanner.scan /.*/
				@_handler.sendSMSRequest [dest], content, (d) => @feedback_sms d
			when 'status'
				for port in @_handler.getStatus()
					@_output.write "Port #{port}: #{status[port].status}\n"
			when 'ussd'
				content = scanner.scan /.*/
				@_handler.sendUSSDRequest content, (d) => @feedback_ussd d
			else
				@_output.write "Unknown command: #{line}\n"

	@feedback = (type, feedback_data) ->
		@_output.write "Feedback for #{type}: #{feedback_data.result_desc}\n"

	@feedback_sms = (d) -> @feedback('SMS', d)
	@feedback_ussd = (d) -> @feedback('USSD', d)

	this
